#!/usr/bin/env python3

import pygame
import sys
import os
import math

class Sprite(pygame.sprite.Sprite):
    rect : pygame.rect.Rect
    image : pygame.Surface

    def __init__(self, filename):
        pygame.sprite.Sprite.__init__(self)
        self.image = self.load(filename)
        self.rect = self.image.get_rect()

    def load(self, name):
        fullname = os.path.join('assets', 'sprites', name)
        try:
            image = pygame.image.load(fullname)
            if image.get_alpha is None:
                image = image.convert()
            else:
                image = image.convert_alpha()
            return image
        except pygame.error:
            print('Cannot load image:', fullname)
            raise SystemExit

    def get_rect(self):
        return self.rect

    def set_rect(self, rect):
        self.rect = rect

    def get_image(self):
        return self.image

    def get_center(self):
        return self.get_rect().center

    def get_width(self):
        return self.get_rect().width

    def get_height(self):
        return self.get_rect().height

class Floor(Sprite):
    def __init__(self, filename):
        Sprite.__init__(self, filename)
        pygame.sprite.RenderPlain(self)

class Bar(Sprite):
    speed : float

    def __init__(self, filename):
        Sprite.__init__(self, filename)
        pygame.sprite.RenderPlain(self)

    def move(self, dx):
        self.set_rect(self.get_rect().move(dx, 0))

    def set_pos(self, x):
        self.set_rect(pygame.rect.Rect(x, self.get_rect().top, self.get_width(), self.get_height()))

class Ball(Sprite):
    rect : pygame.rect.Rect
    image : pygame.Surface
    direction : pygame.Vector2
    angle : float
    speed : float

    def __init__(self, filename):
        Sprite.__init__(self, filename)
        pygame.sprite.RenderPlain(self)

    def set_angle(self, angle : float):
        self.angle = angle

    def get_angle(self):
        return self.angle

    def set_speed(self, speed : float):
        self.speed = speed

    def get_speed(self):
        return self.speed

    def update(self, *args, **kwargs):
        self.direction = pygame.Vector2(self.get_speed() * math.cos(self.get_angle()), self.get_speed() * math.sin(self.get_angle()))
        self.set_rect(self.get_rect().move(self.direction.x, self.direction.y))

class Game:
    surface : pygame.Surface
    stopped : bool = False
    clock : pygame.time.Clock
    score : int
    font : pygame.font.Font
    ball : Ball
    bar : Bar
    floor : Floor
    background : pygame.Surface
 
    def __init__(self, window_title : str, canvas_size : tuple, background_color : tuple = (0, 0, 0)):
        super().__init__()
        self.score = 0
        pygame.init()
        pygame.display.set_caption(window_title)
        pygame.mouse.set_visible(False)
        self.font = pygame.font.SysFont('Courier New', 20, True)
        self.surface = pygame.display.set_mode(canvas_size)
        self.clock = pygame.time.Clock()
        self.load_assets()
        self.init_background(background_color)
        self.ball.set_rect(pygame.rect.Rect(128, 64, self.ball.get_width(), self.ball.get_height()))
        self.ball.set_angle(-0.47)
        self.ball.set_speed(11)
        self.bar.set_rect(pygame.rect.Rect(32, self.background.get_height() - self.floor.get_height() - self.bar.get_height() - 16, self.bar.get_width(), self.bar.get_height()))
        self.floor.image = pygame.transform.scale(self.floor.image, (self.background.get_width(), self.floor.get_height())) # stretch to background width
        self.floor.set_rect(pygame.rect.Rect(0, self.background.get_height() - self.floor.get_height(), self.background.get_width(), self.floor.get_height()))
        self.surface.blit(self.background, (0, 0))
        pygame.display.flip()

    def load_assets(self):
        self.ball = Ball('ball.png')
        self.bar = Bar('bar.png')
        self.floor = Floor('floor.png')

    def init_background(self, color):
        self.background = pygame.Surface(self.surface.get_size())
        self.background = self.background.convert()
        self.background.fill(color)

    def stop(self):
        self.stopped = True

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.stop()
            elif event.type == pygame.MOUSEMOTION:
                (dx, dy) = pygame.mouse.get_rel()
                self.bar.move(dx)
            # TODO: Keep mouse in window

    def render(self):
        # clear everything
        self.surface.blit(self.background, pygame.rect.Rect(self.background.get_rect().left, self.bar.get_rect().top, self.background.get_width(), self.bar.get_height()), pygame.rect.Rect(self.background.get_rect().left, self.bar.get_rect().top, self.background.get_width(), self.bar.get_height())) # Seems not to work. TODO: Find out, what's wrong here.
        self.surface.blit(self.background, self.ball.get_rect(), self.ball.get_rect())
        
        # check, if ball is outside of background
        collision_topleft = not self.background.get_rect().collidepoint(self.ball.get_rect().topleft)
        collision_topright = not self.background.get_rect().collidepoint(self.ball.get_rect().topright)
        collision_bottomleft = not self.background.get_rect().collidepoint(self.ball.get_rect().bottomleft)
        collision_bottomright = not self.background.get_rect().collidepoint(self.ball.get_rect().bottomright)

        if collision_bottomleft and collision_topleft or collision_topright and collision_bottomright:
            self.ball.set_angle(math.pi - self.ball.get_angle()) # bounce
        elif collision_topleft and collision_topright:
            self.ball.set_angle(-1 * self.ball.get_angle()) # bounce
        elif collision_bottomleft and collision_bottomright:
            gameover_text : pygame.Surface = self.font.render('Game over!', True, (255, 255, 0))
            gameover_text_rect = gameover_text.get_rect()
            gameover_text_rect.center = self.background.get_rect().center
            
            self.surface.blit(gameover_text, gameover_text_rect)

            pygame.display.flip()
            return # lost the game

        collision_bottomleft = self.bar.get_rect().collidepoint(self.ball.get_rect().bottomleft)
        collision_bottomright = self.bar.get_rect().collidepoint(self.ball.get_rect().bottomright)

        if collision_bottomleft or collision_bottomright:
            self.ball.set_angle(-1 * self.ball.get_angle()) # bounce
            self.score = self.score + 1
            if self.score % 5 == 0:
                self.ball.set_speed(self.ball.get_speed() * 1.1)

        self.ball.update()

        collision_topleft = not self.background.get_rect().collidepoint(self.bar.get_rect().topleft)
        collision_topright = not self.background.get_rect().collidepoint(self.bar.get_rect().topright)

        if collision_topleft:
            self.bar.set_pos(0) # stop on left border
        elif collision_topright:
            self.bar.set_pos(self.background.get_width() - self.bar.get_width()) # stop on right border

        # draw everything

        for group in self.ball.groups():
            group.draw(self.surface)

        for group in self.bar.groups():
            group.draw(self.surface)

        for group in self.floor.groups():
            group.draw(self.surface)

        score_text : pygame.Surface = self.font.render(str(self.score), True, (255, 0, 255)) # TODO: Prefix with zeros.
        score_text_rect = score_text.get_rect().move(5, 5)
        
        self.surface.blit(self.background, score_text_rect, score_text_rect)
        self.surface.blit(score_text, score_text_rect)

        pygame.display.flip()

    def loop(self, frames_per_second : int = 60):
        self.stopped = False
        while not self.stopped:
            self.clock.tick(frames_per_second)
            self.handle_events()
            self.render()

if __name__ == '__main__':
    game = Game('Pyng-Pong', (800, 800))
    game.loop()
